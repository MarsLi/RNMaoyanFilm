import React from 'react';
import {TouchableHighlight, Image, Text, View, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    },
    icon: {
        width: 30,
        height: 30
    },
    item: {
        alignItems: 'center'
    }
});

export default class TabBarItem extends React.Component {
    render() {
        return (
            <TouchableHighlight
                onPress={this.props.press}
                style={styles.container}
                underlayColor="#b5b5b5">
                <View style={styles.item}>
                    <Image style={styles.icon} source={this.props.image}/>
                    <Text style={styles.text}>{this.props.name}</Text>
                </View>
            </TouchableHighlight>
        );
    }

}

