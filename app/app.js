import React, {Component} from 'react';
import {
    Navigator, Text, View, AppRegistry, Image,
    StyleSheet
} from 'react-native';
import Film from './component/Film'
import Cinema from './component/Cinema'
import Profile from './component/Profile'
import TabBarItem from './util/TabBarItem'

var routeIndex = 0;

var ROUTE_STACK = [
    {
        name: '影片',
        index: 0,
        component: Film
    },
    {
        name: '影院',
        index: 0,
        component: Cinema
    },
    {
        name: '我',
        index: 0,
        component: Profile
    },
];

class App extends Component {

    constructor(props, params) {
        super(props);
        this.state = {
            tabIndex: 0
        }
    }

    render() {
        return (
            <Navigator
                initialRoute={ROUTE_STACK[routeIndex]}
                renderScene={this.renderScene}
                navigationBar={this.TabBar()}
            />
        );
    }

    renderScene(router, navigator) {
        let Com = ROUTE_STACK[routeIndex].component;
        return <Com {...router.params}/>
    }

    TabBar() {
        return (
            <View style={styles.tabs}>
                <TabBarItem
                    name="影片"
                    image={require("./images/film.png")}
                    press={() => {
                        routeIndex = 0;
                        this.setState({tabIndex:0})
                        }
                    }
                />
                <TabBarItem
                    name="影院"
                    image={require("./images/cinema.png")}
                    press={() => {
                        routeIndex = 1;
                        this.setState({tabIndex:1})
                        }
                    }
                />
                <TabBarItem
                    name="我"
                    image={require("./images/me.png")}
                    press={() => {
                        routeIndex = 2;
                        this.setState({tabIndex:2})
                        }
                    }
                />
            </View>);
    }
}

const styles = StyleSheet.create({
    tabs: {
        flexDirection: "row",
        backgroundColor:'#eee',
        borderWidth:2,
        borderColor:'#ddd'
    }
});
export default App