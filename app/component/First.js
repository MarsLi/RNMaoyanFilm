import React from 'react';
import {
    AppRegistry,
    Text,
    Button,
    Navigator
} from 'react-native';

import Second from './SecondPageComponent.js'

export default class First extends React.Component {

    render() {
        let nav = this.props.nav;
        if (nav) {
            return <Text onPress={(event)=>{
            nav.push({name:'first',
                component:Second});
            }
            }> click me to jump</Text>
        }
        return <Text>hello</Text>
    }

}