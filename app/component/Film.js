import React from 'react';
import {
    AppRegistry,
    Text,
    Button,
    Navigator
} from 'react-native';

import FilmList from './FilmList'
export default class Film extends React.Component {

    render() {
        return (
            <Navigator
                initialRoute={{name:'filmList',component:FilmList}}
                renderScene={(route,navigator)=>{
                    let Com = route.component;
                    return <Com {...route.params} navigator = {navigator}/>
                }}
            />
        );
    }

}