import React from 'react';
import {ListView, TouchableOpacity, TouchableHighlight, View, Image, Text, Button} from 'react-native';

export default class FilmList extends React.Component {

    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds,
            dataList: []
        };
    }

    componentDidMount() {
        this.fetchData();
    }

    render() {
        return (
            this.getListView()
        );
    }

    getListView() {
        return (
            <ListView
                dataSource={this.state.dataSource}
                renderRow={(film,sec,i)=>this.getFilm(film,i)
                }
            />);
    }

    fetchData() {
        let url = "http://m.maoyan.com/movie/list.json?type=hot&offset=0&limit=1000";
        let newData = [];
        var data = fetch(url).then((response) => response.json())
            .then((responseData) => {
                responseData.data.movies.map((item) => {
                    console.log(item);
                    item.color = '#d35d5a'
                    return item;
                });
                this.setState({
                    dataSource: this.state.dataSource.cloneWithRows(responseData.data.movies),
                    dataList: responseData.data.movies
                });
            });
    }

    renderFilm(film, index) {
        return (this.getFilm(film, index))
    }

    getFilm(film, index) {
        return (
            <View style={{flexDirection:'row',marginTop:5,marginBottom:5}}>
                {this.getFilmImg(film)}
                {this.getFilmDesc(film)}
                {this.getBuyTicketBtn(film, index)}
            </View>)
    }

    getFilmImg(film) {
        return (
            <View style={{marginRight:10,marginLeft:10}}>
                <Image source={{uri:film.img}} style={{height:100,width: 75,borderRadius:2} }/>
            </View>)
    }

    getFilmDesc(film) {
        return (
            <View style={{flex: 1,justifyContent:'center'}}>
                <Text style={{flex:1,fontSize:17,color:'black'}}>{film.nm}</Text>
                {this.renderWishOrScore(film)}
                <Text style={
                    {flex:1,fontSize: 15,color:'#828282'}}>{film.scm}</Text>
                <Text style={
                    {flex:1}}>{film.showInfo}</Text>
            </View>
        )
    }

    getBuyTicketBtn(film, index) {
        return (
            <View style={{justifyContent:'center'}}>
                <TouchableHighlight
                    style={
                        {
                            width:50,
                            marginRight:7,
                            marginLeft:7,
                            borderColor:'#d35d5a',
                            alignItems:'center',
                            borderWidth:1
                        }
                    }
                    underlayColor="#d35d5a"
                    onPress={()=>{alert('you are going to buy tickets!')}}
                    onHideUnderlay={()=>{
                        let newArray = this.state.dataList.slice();
                        let newItem = JSON.parse(JSON.stringify(newArray[index]));
                        newItem.color = '#d35d5a';
                        newArray[index] = newItem;
                        this.setState({dataSource:this.state.dataSource.cloneWithRows(newArray)})
                    }}
                    onShowUnderlay={()=>{
                        let newArray = this.state.dataList.slice();
                        let newItem = JSON.parse(JSON.stringify(newArray[index]));
                        newItem.color = 'white';
                        newArray[index] = newItem;
                        this.setState({dataSource:this.state.dataSource.cloneWithRows(newArray)})
                    }}
                >
                    <Text style={{color:film.color
                }}>{film.preSale ? "预购" : "购票"}</Text>
                </TouchableHighlight>
            </View>
        )
    }

    renderWishOrScore(film) {
        if (film.sc > 0) {
            return <Text style={
                {flex:1,fontSize: 14,color:'darkgray'}}>观众 <Text style={{fontSize: 16,color:'#e8c836'}}>{film.sc}
            </Text>
            </Text >
        } else if (film.wish > 0) {
            return <Text style={{fontSize: 14,color:'#e8c836'} }>{film.wish} <Text style={
                {flex:1,fontSize: 14,color:'darkgray'}}>人想看
            </Text>
            </Text >
        }

        return '';
    }
}