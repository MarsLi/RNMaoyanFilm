#!/usr/bin/env bash
JENKINS_BUILD_NUMBER=$1
ENV=`echo "$2" | tr "[:upper:]" "[:lower:]"`

MOBILE_FILE_PATH="/Users/genli/Documents/CI/app-debug.apk"
PLIST_FILE_PATH=""
APP_ID=1

function getGitLog(){
    #Get first 100 git commit log
    echo $(git log \
        -100 \
        --pretty=format:'{%n  "commit": "%H",%n  "author": "%aN <%aE>",%n  "date": "%ad",%n  "message": "%f"%n},' \
        $@ | \
        perl -pe 'BEGIN{print "["}; END{print "]\n"}' | \
        perl -pe 's/},]/}]/')
}

gitLog= getGitLog
echo ${gitLog}
curl   -X POST \
       -H "Content-Type: multipart/form-data" \
       -F "buildNo=$JENKINS_BUILD_NUMBER" \
       -F "versionType=android" \
       -F "env=$ENV" \
       -F "mobileFile=@$MOBILE_FILE_PATH" \
       -F "gitHistory=$gitLog" \
       http://54.222.131.195:8090/api/apps/${APP_ID}/versions
